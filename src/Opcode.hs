module Opcode (Opcode(..), toMnemonic, fromMnemonic, toByte, fromByte) where

-- | An operation code.
data Opcode
    = Duplicate
    | Drop
    | Swap
    | Over
    | DataToReturn
    | ReturnToData
    | Add
    | Subtract
    | Multiply
    | Divide
    | Negate
    | Absolute
    | And
    | Or
    | Xor
    | ShiftLeft
    | ShiftRight
    | Branch
    | BranchOnNegative
    | BranchOnZero
    | BranchOnPositive
    | Call
    | Return
    | Load
    | Save
    | Input
    | OutputNumber
    | OutputCharacter
    | LoadProgramCounter
    | Depth
    | NoOperation
    | Halt
    deriving (Eq, Ord, Enum, Bounded, Show)

-- | Get the mnenomic for the given opcode.
--
-- >>> toMnemonic DataToReturn
-- "DTR"
toMnemonic :: Opcode -> String
toMnemonic Duplicate = "DUP"
toMnemonic Drop = "DROP"
toMnemonic Swap = "SWAP"
toMnemonic Over = "OVER"
toMnemonic DataToReturn = "DTR"
toMnemonic ReturnToData = "RTD"
toMnemonic Add = "ADD"
toMnemonic Subtract = "SUB"
toMnemonic Multiply = "MUL"
toMnemonic Divide = "DIV"
toMnemonic Negate = "NEG"
toMnemonic Absolute = "ABS"
toMnemonic And = "AND"
toMnemonic Or = "OR"
toMnemonic Xor = "XOR"
toMnemonic ShiftLeft = "SHL"
toMnemonic ShiftRight = "SHR"
toMnemonic Branch = "BR"
toMnemonic BranchOnNegative = "BRN"
toMnemonic BranchOnZero = "BRZ"
toMnemonic BranchOnPositive = "BRP"
toMnemonic Call = "CALL"
toMnemonic Return = "RET"
toMnemonic Load = "LOAD"
toMnemonic Save = "SAVE"
toMnemonic Input = "IN"
toMnemonic OutputNumber = "OUTN"
toMnemonic OutputCharacter = "OUTC"
toMnemonic LoadProgramCounter = "LPC"
toMnemonic Depth = "DEPTH"
toMnemonic NoOperation = "NOP"
toMnemonic Halt = "HALT"

-- | Get the opcode with the given mnemonic.
--
-- >>> fromMnemonic "DUP"
-- Just Duplicate
-- >>> fromMnemonic "WTF"
-- Nothing
fromMnemonic :: String -> Maybe Opcode
fromMnemonic mnemonic = lookup mnemonic table
    where table = fmap (\opcode -> (toMnemonic opcode, opcode)) [Duplicate ..]

-- | Convert an opcode to a byte.
--
-- >>> toByte Duplicate
-- -1
-- >>> toByte Halt
-- -32
toByte :: Opcode -> Int
toByte opcode = -(fromIntegral (fromEnum opcode) + 1)

-- | Convert a byte to an opcode.
--
-- >>> fromByte (-1)
-- Just Duplicate
-- >>> fromByte (-32)
-- Just Halt
-- >>> fromByte (-40)
-- Nothing
fromByte :: Int -> Maybe Opcode
fromByte byte
    | isInBounds = Just opcode
    | otherwise = Nothing
  where
    opcode' = -byte - 1
    opcode = toEnum (fromIntegral opcode')

    isInBounds = minBound' <= opcode' && opcode' <= maxBound'
    minBound' = fromIntegral (fromEnum (minBound :: Opcode))
    maxBound' = fromIntegral (fromEnum (maxBound :: Opcode))
