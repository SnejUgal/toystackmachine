{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}

module StackMachine
    ( StackMachine(cycles, dataStack, memory, programCounter, returnStack)
    , Error(..)
    , initialize
    , executeCycle
    , execute
    ) where

import Control.Monad (when)
import Control.Monad.Trans.Except (ExceptT, runExceptT, throwE)
import Control.Monad.Trans.State (StateT(StateT), get, put, runStateT)

import Data.Bits ((.&.), (.|.), xor)
import Data.Vector ((!?), (//), Vector)
import qualified Data.Vector as Vector

import Control.Monad.Trans.Class (MonadTrans(lift))
import Opcode (Opcode(..))
import qualified Opcode

-- | A stack machine.
data StackMachine m = StackMachine
    { memory :: Vector Int
    , dataStack :: [Int]
    , returnStack :: [Int]
    , programCounter :: Int
    , cycles :: Integer
    , input :: Execution m Int
    , output :: Char -> Execution m ()
    }

instance Show (StackMachine m) where
    show StackMachine { memory, dataStack, returnStack, programCounter, cycles } =
        "StackMachine {\n"
            ++ ("  memory = " ++ show memory ++ ",\n")
            ++ ("  dataStack = " ++ show (reverse dataStack) ++ ",\n")
            ++ ("  returnStack = " ++ show (reverse returnStack) ++ ",\n")
            ++ ("  programCounter = " ++ show programCounter ++ ",\n")
            ++ ("  cycles = " ++ show cycles ++ ",\n")
            ++ "}"

-- | Errors that may happen while the stack machine executes.
data Error
  = AddressOutOfBounds Int
  | UnknownInstruction Int
  | ShortDataStack
  | ShortReturnStack
  deriving (Eq, Show)

data OpcodeExecution = Continued | Jumped | Halted deriving (Eq)

-- | Initialize a stack machine.
initialize
    :: Monad m
    => m Int -- ^ Read a number
    -> (Char -> m ()) -- ^ Print a character
    -> Vector Int -- ^ Bytecode
    -> StackMachine m
initialize input output memory = StackMachine
    { memory
    , dataStack = []
    , returnStack = []
    , programCounter = 0
    , cycles = 0
    , input = lift' input
    , output = lift' . output
    }
    -- First lift from `m` to `ExceptT`, then to `StateT`
    where lift' = lift . lift

-- | Execute one cycle. Returns whether the stack machine has halted.
executeCycle :: Monad m => StackMachine m -> m (Either Error Bool, StackMachine m)
executeCycle machine = runExceptT (runStateT executeCycle' machine) >>= \case
    Left (err, machine') -> return (Left err, machine')
    Right (result, machine') -> return (Right (result == Halted), machine')

-- | Execute the program until it stops.
execute :: Monad m => StackMachine m -> m (Either Error (), StackMachine m)
execute machine = runExceptT (runStateT execute' machine) >>= \case
    Left (err, machine') -> return (Left err, machine')
    Right ((), machine') -> return (Right (), machine')

-- Stack machine internals
-- -----------------------

type Execution m = StateT (StackMachine m) (ExceptT (Error, StackMachine m) m)

throw :: Monad m => Error -> Execution m a
throw err = StateT (\state -> throwE (err, state))

-- | Read the memory cell at the given address.
readCell :: Monad m => Int -> Execution m Int
readCell address = do
    StackMachine { memory } <- get
    case memory !? address of
        Nothing -> throw (AddressOutOfBounds address)
        Just cell -> return cell

-- | Write a value to a memory cell with the given address.
writeCell :: Monad m => Int -> Int -> Execution m ()
writeCell value address = do
    machine@StackMachine { memory } <- get
    if address >= Vector.length memory
        then throw (AddressOutOfBounds address)
        else do
            let memory' = memory // [(address, value)]
            put machine { memory = memory' }

-- | Get the data stack.
getData :: Monad m => Execution m [Int]
getData = do
    StackMachine { dataStack } <- get
    return dataStack

-- | Put an updated data stack.
putData :: Monad m => [Int] -> Execution m ()
putData dataStack = do
    machine <- get
    put machine { dataStack }

-- | Peek the top of the data stack.
peekData :: Monad m => Execution m Int
peekData = getData >>= \case
    [] -> throw ShortDataStack
    value : _ -> return value

-- | Push a value to the data stack.
pushData :: Monad m => Int -> Execution m ()
pushData value = do
    dataStack <- getData
    putData (value : dataStack)

-- | Pop a value off the data stack.
popData :: Monad m => Execution m Int
popData = getData >>= \case
    [] -> throw ShortDataStack
    value : rest -> do
        putData rest
        return value

-- | Get the return stack.
getReturn :: Monad m => Execution m [Int]
getReturn = do
    StackMachine { returnStack } <- get
    return returnStack

-- | Put an updated return stack.
putReturn :: Monad m => [Int] -> Execution m ()
putReturn returnStack = do
    machine <- get
    put machine { returnStack }

-- | Push an address to the return stack.
pushReturn :: Monad m => Int -> Execution m ()
pushReturn address = do
    returnStack <- getReturn
    putReturn (address : returnStack)

-- | Pop an address off the return stack.
popReturn :: Monad m => Execution m Int
popReturn = getReturn >>= \case
    [] -> throw ShortReturnStack
    address : rest -> do
        putReturn rest
        return address

-- | Return the program counter.
getProgramCounter :: Monad m => Execution m Int
getProgramCounter = do
    StackMachine { programCounter } <- get
    return programCounter

-- | Put an updated program counter.
putProgramCounter :: Monad m => Int -> Execution m ()
putProgramCounter programCounter = do
    machine <- get
    put machine { programCounter }

-- | Increment the program counter.
incrementProgramCounter :: Monad m => Execution m ()
incrementProgramCounter = do
    programCounter <- getProgramCounter
    putProgramCounter (programCounter + 1)

-- | Increment the counter of executed cycles.
incrementCycles :: Monad m => Execution m ()
incrementCycles = do
    machine@StackMachine { cycles } <- get
    put machine { cycles = cycles + 1 }

-- | Output a string.
outputString :: Monad m => String -> Execution m ()
outputString [] = return ()
outputString (character : rest) = do
    StackMachine { output } <- get
    output character
    outputString rest

-- | Execute an opcode on the machine.
executeOpcode :: Monad m => Opcode -> Execution m OpcodeExecution

executeOpcode Duplicate = do
    value <- peekData
    pushData value
    return Continued

executeOpcode Drop = do
    _ <- popData
    return Continued

executeOpcode Swap = do
    b <- popData
    a <- popData
    pushData b
    pushData a
    return Continued

executeOpcode Over = do
    b <- popData
    a <- popData
    pushData a
    pushData b
    pushData a
    return Continued

executeOpcode DataToReturn = do
    popData >>= pushReturn
    return Continued

executeOpcode ReturnToData = do
    popReturn >>= pushData
    return Continued

executeOpcode Add = do
    b <- popData
    a <- popData
    pushData (a + b)
    return Continued

executeOpcode Subtract = do
    b <- popData
    a <- popData
    pushData (a - b)
    return Continued

executeOpcode Multiply = do
    b <- popData
    a <- popData
    pushData (a * b)
    return Continued

executeOpcode Divide = do
    b <- popData
    a <- popData
    pushData (a `mod` b)
    pushData (a `div` b)
    return Continued

executeOpcode Negate = do
    x <- popData
    pushData (-x)
    return Continued

executeOpcode Absolute = do
    x <- popData
    pushData (abs x)
    return Continued

executeOpcode And = do
    b <- popData
    a <- popData
    pushData (a .&. b)
    return Continued

executeOpcode Or = do
    b <- popData
    a <- popData
    pushData (a .|. b)
    return Continued

executeOpcode Xor = do
    b <- popData
    a <- popData
    pushData (a `xor` b)
    return Continued

executeOpcode ShiftLeft = do
    x <- popData
    pushData (x * 2)
    return Continued

executeOpcode ShiftRight = do
    x <- popData
    pushData (x `div` 2)
    return Continued

executeOpcode Branch = do
    address <- popData
    putProgramCounter (fromIntegral address)
    return Jumped

executeOpcode BranchOnNegative = do
    address <- popData
    flag <- popData
    if flag < 0
        then do
            putProgramCounter (fromIntegral address)
            return Jumped
        else return Continued

executeOpcode BranchOnZero = do
    address <- popData
    flag <- popData
    if flag == 0
        then do
            putProgramCounter (fromIntegral address)
            return Jumped
        else return Continued

executeOpcode BranchOnPositive = do
    address <- popData
    flag <- popData
    if flag > 0
        then do
            putProgramCounter (fromIntegral address)
            return Jumped
        else return Continued

executeOpcode Call = do
    address <- popData
    programCounter <- getProgramCounter
    pushReturn (fromIntegral programCounter + 1)
    putProgramCounter (fromIntegral address)
    return Jumped

executeOpcode Return = do
    address <- popReturn
    putProgramCounter (fromIntegral address)
    return Jumped

executeOpcode Load = do
    address <- popData
    value <- readCell (fromIntegral address)
    pushData value
    return Continued

executeOpcode Save = do
    address <- popData
    value <- popData
    writeCell value (fromIntegral address)
    return Continued

executeOpcode Input = do
    StackMachine { input } <- get
    input >>= pushData
    return Continued

executeOpcode OutputNumber = do
    number <- popData
    outputString (show number)
    return Continued

executeOpcode OutputCharacter = do
    character <- popData
    outputString [toEnum (fromIntegral character)]
    return Continued

executeOpcode LoadProgramCounter = do
    programCounter <- getProgramCounter
    pushData (fromIntegral programCounter)
    return Continued

executeOpcode Depth = do
    dataStack <- getData
    pushData (fromIntegral (length dataStack))
    return Continued

executeOpcode NoOperation = return Continued
executeOpcode Halt = return Halted

-- | Execute one cycle on the machine.
executeCycle' :: Monad m => Execution m OpcodeExecution
executeCycle' = do
    instruction <- readCell =<< getProgramCounter
    if instruction >= 0
        then do
            pushData instruction
            incrementCycles
            incrementProgramCounter
            return Continued
        else case Opcode.fromByte instruction of
            Nothing -> throw (UnknownInstruction instruction)
            Just opcode -> do
                result <- executeOpcode opcode
                incrementCycles
                when (result == Continued) incrementProgramCounter
                return result

-- | Execute the program until it stops.
execute' :: Monad m => Execution m ()
execute' = executeCycle' >>= \case
    Halted -> return ()
    _ -> execute'
