{-# LANGUAGE NamedFieldPuns #-}

module Assembler (assemble, Error(..)) where

import Data.Char (isSpace, toUpper)
import Data.Maybe (fromMaybe)
import Data.Vector (Vector)
import qualified Data.Vector as Vector
import Text.Read (readMaybe)

import Control.Applicative (Alternative((<|>)))

import Opcode (Opcode)
import qualified Opcode

import Data.Map (Map)
import qualified Data.Map as Map
import Prelude hiding (lines)

-- | Errors that may happen during assembly process.
data Error
    = DoubleLabel
    | ExpectedValue
    | DuplicateLabel
    | EmptyLabel
    | UnknownLabel String
    deriving (Eq, Show)

data Value
    = Literal Int
    | Opcode Opcode
    | LabelRef String
    deriving (Eq, Show)

data Token
    = Value Value
    | Label String
    deriving (Eq, Show)

data Line = Line
    { value :: Value
    , label :: Maybe String
    }
    deriving (Eq, Show)

type SymbolTable = Map String Int

-- | Assemble the code.
-- 
-- >>> assemble ":loop 97 OUTC loop ; a comment\n br"
-- Right [97,-28,0,-18]
assemble :: String -> Either Error (Vector Int)
assemble code = do
    let tokens = tokenize code
    lines <- toLines tokens
    symbols <- collectSymbols lines
    bytecode <- serialize symbols lines
    return (Vector.fromList bytecode)

-- | Serialize lines resolving all labels.
serialize :: SymbolTable -> [Line] -> Either Error [Int]
serialize table = mapM (serializer . value)
  where
    serializer (Literal literal) = Right literal
    serializer (Opcode opcode) = Right (Opcode.toByte opcode)
    serializer (LabelRef label) = case Map.lookup label table of
        Nothing -> Left (UnknownLabel label)
        Just address -> Right address

-- | Collect symbols from the lines.
collectSymbols :: [Line] -> Either Error SymbolTable
collectSymbols lines = foldr collector (Right Map.empty) (enumerate lines)
  where
    enumerate = zip [0 ..]
    collector _ (Left err) = Left err
    collector (index, Line { label }) (Right table) = case label of
        Nothing -> Right table
        Just "" -> Left EmptyLabel
        Just label'
            | Map.member label' table -> Left DuplicateLabel
            | otherwise -> Right (Map.insert label' index table)

-- | Unite tokens into `Line`s.
toLines :: [Token] -> Either Error [Line]
toLines [] = Right []
toLines (Value value : rest) = do
    nextLines <- toLines rest
    return (Line { value, label = Nothing } : nextLines)
toLines (Label label : Value value : rest) = do
    nextLines <- toLines rest
    return (Line { value, label = Just label } : nextLines)
toLines (Label _ : Label _ : _) = Left DoubleLabel
toLines [Label _] = Left ExpectedValue

-- | Split the code into tokens.
tokenize :: String -> [Token]
tokenize code = case extractToken code of
    Nothing -> []
    Just (token, next) -> token : tokenize next

-- | Extract one token from the source code.
extractToken :: String -> Maybe (Token, String)
extractToken [] = Nothing

-- Skip whitespace
extractToken (character : code) | isSpace character = extractToken (dropWhile isSpace code)

-- Comments
extractToken (start : code) | start == ';' || start == '#' = extractToken next
    where next = dropWhile (/= '\n') code

-- Label declarations
extractToken (':' : code) = Just (Label label, next)
  where
    (label', next) = break isSpace code
    label = map toUpper label'

-- A value
extractToken code = Just (Value value, next)
  where
    (identifier, next) = break isSpace code
    value = fromMaybe
        (parseLabelRef identifier)
        (parseLiteral identifier <|> parseOpcode identifier)

parseLiteral :: String -> Maybe Value
parseLiteral = fmap Literal . readMaybe

parseOpcode :: String -> Maybe Value
parseOpcode = fmap Opcode . Opcode.fromMnemonic . map toUpper

parseLabelRef :: String -> Value
parseLabelRef = LabelRef . map toUpper
