{-# LANGUAGE LambdaCase #-}

module Main (main) where

import Control.Monad.Trans.Class (MonadTrans(lift))
import Control.Monad.Trans.Except (ExceptT, runExceptT, throwE)

import System.Environment (getArgs)
import System.Exit (exitFailure)

import qualified Assembler
import Assembler (assemble)
import qualified StackMachine

data Error
    = MissingFilePath
    | AssemblyError Assembler.Error
    | ExecutionError StackMachine.Error
    deriving (Eq, Show)

input :: ExceptT Error IO Int
input = lift (read <$> getLine)

output :: Char -> ExceptT Error IO ()
output = lift . putChar

run :: ExceptT Error IO ()
run = do
    filePath <- lift getArgs >>= \case
        [] -> throwE MissingFilePath
        (filePath : _) -> return filePath
    code <- lift (readFile filePath)

    bytecode <- case assemble code of
        Left err -> throwE (AssemblyError err)
        Right bytecode -> return bytecode

    let machine = StackMachine.initialize input output bytecode
    (result, machine') <- StackMachine.execute machine

    lift $ do
        putStrLn ""
        case result of
            Right () -> putStrLn "Machine halted"
            Left err -> putStrLn ("Machine errored with " ++ show err)
        print machine'

main :: IO ()
main = runExceptT run >>= \case
    Left err -> do
        putStrLn ("Error: " ++ show err)
        exitFailure
    Right () -> return ()
