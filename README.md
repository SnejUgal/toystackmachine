# A toy stack machine

An implementation of the stack machine described in the book “The stack, or a
trip there and back” by Alexey Vtornikov.

While the author implemented the stack machine in Java, this repository
contains an implementation in Haskell. It can be used both as a library and an
executable.

## Executing a program on the machine

To execute a program on this stack machine, run `stack run` passing the path to
the program's assembly:

```sh
stack run path/to/the/program.asm
```

The repository contains an example program which computes the greatest common
divisor of two entered numbers:

```sh
$ stack run programs/gcd.asm
50
45
5
Machine halted
StackMachine {
  memory = [-26,-26,6,-22,-27,-32,-1,15,-20,-3,-4,-10,-2,6,-18,-2,-23],
  dataStack = [],
  returnStack = [],
  programCounter = 5,
  cycles = 29,
}
```

## Differences from the author's implementation

This implementation's assembly syntax is a bit more permissive: any whitespace
between two instructions is enough.

The stack machine uses different numbers for some instructions. As such, this
implementation's bytecode is incompatible with the author's.
