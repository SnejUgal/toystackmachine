; Compute the greatest common divisor of two numbers

        IN
        IN
        euclid
        CALL
        OUTN
        HALT

:euclid DUP
        done
        BRZ
        SWAP
        OVER
        DIV
        DROP
        euclid
        BR
:done   DROP
        RET
